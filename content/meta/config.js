const colors = require("../../src/styles/colors");

module.exports = {
  siteTitle: "Deep blog", // <title>
  shortSiteTitle: "Deep blog", // <title> ending for posts and pages
  siteDescription: "Deep blog.",
  siteUrl: "https://gatsby-starter-personal-blog.greglobinski.com",
  pathPrefix: "",
  siteImage: "preview.jpg",
  siteLanguage: "en",
  // author
  authorName: "AI research team",
  authorTwitterAccount: "",
  // info
  infoTitle: "NBT",
  infoTitleNote: "research team",
  // manifest.json
  manifestName: "Deep blog - a blog for deep learning.",
  manifestShortName: "Deep blog", // max 12 characters
  manifestStartUrl: "/",
  manifestBackgroundColor: colors.background,
  manifestThemeColor: colors.background,
  manifestDisplay: "standalone",
  // contact
  contactEmail: "sine.scipy@gmail.com",
  // social
  authorSocialLinks: [
    { name: "github", url: "" },
    { name: "twitter", url: "" },
    { name: "facebook", url: "" }
  ]
};
